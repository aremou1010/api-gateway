FROM openjdk:8-jdk-alpine
MAINTAINER aremou.adjado@gmail.com
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
#ENTRYPOINT ["/usr/bin/java"]
#ENTRYPOINT ["/usr/bin/"]
ENTRYPOINT ["java","-jar","app.jar"]
EXPOSE 8111
